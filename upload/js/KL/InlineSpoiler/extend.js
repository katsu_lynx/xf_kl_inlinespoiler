/**
 * KL_InlineSpoiler_extend
 *
 *	@author: Katsulynx
 *  @last_edit:	14.09.2015
 *	@compiled:	19.05.2016 - Google Closure Compiler
 */
if("undefined"==typeof RedactorPlugins)var RedactorPlugins={};!function(a,e,b,f){a(b).on("EditorInit",function(b,c){var d={inlineSpoiler:{callback:function(a,b){a.execCommand("inserthtml","[iSpoiler]"+a.getSelection()+"[/iSpoiler]");return!1},className:"icon inlineSpoiler",title:RELANG.xf.inlineSpoiler}};console.log(c.editor.editorConfig.buttonsCustom.insert);a.extend(c.editor.editorConfig.buttonsCustom.insert.dropdown,d)})}(jQuery,this,document,"undefined");