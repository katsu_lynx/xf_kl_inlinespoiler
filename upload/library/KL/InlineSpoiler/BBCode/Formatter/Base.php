<?php

/**
 * Class: KL_InlineSpoiler_BbCode_Formatter_Spoiler
 *
 * Inline Spoiler BbCode Handler. Responsible for rendering
 * the spoiler tag and remove unwanted children.
 *
 * Last changed:	09/07/2016
 */

class KL_InlineSpoiler_BbCode_Formatter_Base extends XFCP_KL_InlineSpoiler_BbCode_Formatter_Base {
	
    /** 
	 * Array for cleaning Inline Spoiler children
	 *
     * @var array
     */
	protected $_inlineSpoilerChildren = array('b', 'i', 's', 'ispoiler', 'u');

	/**
	 * Get the list of parsable tags and their parsing rules.
	 * Extend with InlineSpoiler.
	 *
	 * @return array
	 */
	public function getTags() {
		$tags = parent::getTags();
		
		$tags['ispoiler'] = array(
				'hasOption' => false,
				'stopLineBreakConversion' => true,
				'callback' => array($this, 'renderTagInlineSpoiler')
			);
		
		return $tags;
	}

    /** 
	 * Render the inline spoiler tag.
	 *
     * @param array $tag
	 * @param array $rendererStates
	 * @param XenForo_BbCode_Formatter_Base $formatter
	 *
     * @return string
     */
    public function renderTagInlineSpoiler(array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter) {
		/* Resolve children by stripping unwanted tags */
        $tag = $this->_resolveInlineSpoilerChildren($tag);
		$content = $formatter->renderSubTree($tag['children'], $rendererStates);
		
		if ($this->_view)
		{
			$template = $this->_view->createTemplateObject('KL_InlineSpoiler_bb_code_tag_ispoiler', array(
				'content' => $content,
			));
			return $template->render();
		}
		else
		{
			return $this->_renderTagSpoilerFallback(null, $content, $rendererStates);
		}
    }
    
    /* 
	 * Strip unwanted tags from input.
	 * Prunes BB-Code Tree.
	 *
     * @param array $tag
     * @return array
     */
    private function _resolveInlineSpoilerChildren($tag) {
		/* Strip tag and all children if not in allowed tags */
		if(is_array($tag)) {
			if(isset($tag['tag']) && !in_array($tag['tag'], $this->_inlineSpoilerChildren)) {
				return null;
			}
			else if(isset($tag['children'])) {
				/* Do recursive for children */
				foreach($tag['children'] as &$child) {
						$child = $this->_resolveInlineSpoilerChildren($child);
				}
			}
		}
        
		/* Return pruned (Sub-)Tree */
        return $tag;
    }
}